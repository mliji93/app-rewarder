# App rewarder



## Getting started

Project name: AppRewarder

##About project
The current project is a Django-based web application designed to reward users for completing tasks related to downloading and using Android apps. Users can register, log in, and earn points by completing tasks associated with specific apps. The project includes features such as app categories, subcategories, and task completion tracking. The user interface and functionality have been developed, and it's ready for deployment. The project's next steps involve adding additional styling, refining user interactions, and deploying it to a web hosting platform like PythonAnywhere.


#Features


User Registration and Authentication: Allow users to create accounts and log in securely to access the application's features.

App Listing: Display a list of Android apps from a database, including details such as app name, category, sub-category, logo, and points earned.

Task Completion: Enable users to complete tasks related to Android apps. When a user completes a task, they earn points.

Task Submission: Allow users to submit screenshots as proof when they complete a task.

User Profiles: Create user profiles with information like name, phone number, email, and points earned.

Point System: Implement a points system where users earn points for completing tasks. Display the total points earned by each user.

Dashboard: Provide a dashboard for users to view their profiles, earned points, and a list of available tasks.

Admin Panel: Create an admin panel to manage Android apps, user profiles, and user tasks. Admins can add new apps, review user-submitted screenshots, and more.




## Add your files

push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/mliji93/app-rewarder.git
git branch -M main
git push -uf origin main
```


## Installation
# Clone the repository
git clone https://github.com/yourusername/app-rewarder.git

# Change to project directory
cd app-rewarder.git

# Install dependencies
pip install -r requirements.txt

# Set up the database
python manage.py migrate

# Run the development server
python manage.py runserver

##hosting project at Pythonanywere

1. Set Up a PythonAnywhere Web App

Log in to your PythonAnywhere account.

From your dashboard, click on the "Web" tab.

Click the "Add a new web app" button.

Follow the wizard to create a new web app. Choose the "Manual Configuration" option.

2. Clone Your Project Repository

Open a new terminal in PythonAnywhere.

Clone your project's repository into your PythonAnywhere account using Git. For example:


git clone https://github.com/yourusername/AppRewarder.git

3. Change your working directory to the project folder:

cd AppRewarder

4. Set Up a Virtual Environment

Create a virtual environment for your project. Replace yourvenv with your preferred virtual environment name:

mkvirtualenv --python=/usr/bin/python3.8 yourvenv

5. Install Project Dependencies

Activate the virtual environment:

workon yourvenv
Install the project dependencies using pip. You might need to use a requirements file:

pip install -r requirements.txt

6. Configure Your Django Settings

Open  Django project's settings (e.g., settings.py) and make sure to set DEBUG to False for production.

Update your ALLOWED_HOSTS to include your PythonAnywhere domain and any custom domains you plan to use.


7.  Collect Static Files

copy and past static file path, source code path. 

Run the following command to collect static files to a directory that PythonAnywhere can serve:

python manage.py collectstatic

8.  Update Static File URL

In your Django settings, make sure your STATIC_URL is configured to the correct URL where PythonAnywhere serves static files.

9. Set Up WSGI Configuration

Navigate to the "Web" tab on PythonAnywhere.

In the "Code" section, find the "WSGI configuration file" section and click the link to edit it.

Update the WSGI configuration to point to your Django project's wsgi.py file. It might look like this:


import os
import sys

path = '/path/to/your/AppRewarder'
if path not in sys.path:
    sys.path.append(path)

os.environ['DJANGO_SETTINGS_MODULE'] = 'AppRewarder.settings'

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()


10.  Restart Your Web App

Go back to your PythonAnywhere dashboard and find your web app.

Click the "Reload" button to restart your web app with the updated settings.


Step 13: Testing

Visit your PythonAnywhere web app's URL in a web browser to test if your Django project is working correctly.
That's it! Your Django project should now be hosted on PythonAnywhere. You can make further configurations and optimizations based on your project's requirements and security considerations.


## MIT License

Copyright (c) 2023 Liji Mathew

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.


