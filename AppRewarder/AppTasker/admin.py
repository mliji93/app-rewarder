from django.contrib import admin
from .models import UserProfile,UserTask,AndroidApp,UserPoints

# Register your models here.
admin.site.register(UserTask)
admin.site.register(UserProfile)
admin.site.register(AndroidApp)
admin.site.register(UserPoints)


