from django.urls import path
from . import views
from django.contrib.auth import views as auth_views

from django.conf import settings
from django.conf.urls.static import static



urlpatterns = [
    path('',views.index, name='index'),
    path('api/apps/', views.AppListView.as_view(), name='app-list'), 
    path('home/',views.home, name='home'),

    path('login_view/', views.login_view, name='login_view'),
    path('dashboard/', views.dashboard, name='dashboard'),
    path('register/', views.register, name='register'),
    path('android_app_create_form/', views.android_app_create_form, name='android_app_create_form'),
    path('logout/', views.custom_logout, name='logout'),
    path('profile/', views.profile_view, name='profile'),
    path('app_details/<int:app_id>/', views.app_detail, name='app_detail'),
    path('complete_task/<int:app_id>/', views.app_detail, name='complete_task'),
    path('user_points/', views.user_points, name='user_points'),


   

]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)