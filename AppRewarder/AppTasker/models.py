
from django.db import models


class UserProfile(models.Model):
    name= models.CharField(max_length=150 )
    phone =models.CharField(max_length=200)
    email = models.EmailField(max_length=100, default=None)
    type=models.CharField(max_length=100, default=None)
    username=models.CharField(max_length=100)
    password = models.CharField(max_length=150)
  

    def __str__(self):
        return self.name


class AndroidApp(models.Model):
    CATEGORY_CHOICES = (
        ('Social', 'Social'),
        ('Communication', 'Communication'),
        ('Video Players & Editors', 'Video Players & Editors'),
        ('Job Portal', 'Job Portal'),
        ('Entertainment', 'Entertainment'),
    )

    SUB_CATEGORY_CHOICES = (
        ('Streaming', 'Streaming'),
        ('Social Networking', 'Social Networking'),
        ('Photo & Video Sharing', 'Photo & Video Sharing'),
        ('Instant Messaging', 'Instant Messaging'),
        ('Professional Networking', 'Professional Networking'),
    )

    logo = models.ImageField(upload_to='logos/', blank=True, null=True)
    app_name = models.CharField(max_length=100)
    link = models.URLField(max_length=150)
    category = models.CharField(max_length=150, choices=CATEGORY_CHOICES)
    sub_category = models.CharField(max_length=150, choices=SUB_CATEGORY_CHOICES)
    points_earned = models.IntegerField()

    def __str__(self):
        return self.app_name

    
class UserTask(models.Model):
    user = models.ForeignKey(UserProfile, on_delete=models.CASCADE, related_name='tasks', default=None)
    task_name = models.CharField(max_length=200, default=None)
    task_category = models.CharField(max_length=200, default=None)
    task_sub_category = models.CharField(max_length=200, default=None)
    completed = models.BooleanField(default=False)
    screenshot = models.ImageField(upload_to='task_screenshots/', blank=True, null=True)

    def __str__(self):
        return self.task_name



class UserPoints(models.Model):
    user = models.ForeignKey(UserProfile, on_delete=models.CASCADE)
    android_app = models.ForeignKey(AndroidApp, on_delete=models.CASCADE,default=None)
    points_earned = models.IntegerField(default=0)
    total_points = models.IntegerField(default=0)  # Add a total_points field

    def __str__(self):
        return f"{self.user} - {self.android_app.app_name} - Points: {self.points_earned} - Total Points: {self.total_points}"
    
    
