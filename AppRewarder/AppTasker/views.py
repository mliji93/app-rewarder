from django.shortcuts import render,redirect,get_object_or_404
from .forms import AndroidAppForm, RegistrationForm,ScreenshotUploadForm
from django.contrib import messages  
from .models import UserProfile, AndroidApp, UserTask, UserPoints
from .serializers import AndroidAppSerializer
from django.contrib.auth import logout
from django.db.models import Sum
from rest_framework.views import APIView
from rest_framework import generics, status
from rest_framework.response import Response

from rest_framework import status

def index(request):
    return render(request, 'index.html')
def home(request):
    return render(request,'home.html')

def login_view(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']

        try:
            # Retrieve the user from the database based on the username
            user = UserProfile.objects.get(username=username)

            if user.password == password:
                request.session['user_id'] = user.id
                return redirect('dashboard')
            else:
                messages.error(request, 'Invalid username or password')
        except UserProfile.DoesNotExist:
            messages.error(request, 'Invalid username or password')

    return render(request, 'index.html')


def register(request):
    if request.method == 'POST':
        form = RegistrationForm(request.POST)
        if form.is_valid():
            # Automatically set the role to 'user'
            user = form.save(commit=False)
            user.type = 'user'
            
            user.save()
            return redirect('index')  
    else:
        form = RegistrationForm()
    
    return render(request, 'register.html', {'form': form})




def dashboard(request):
    if 'user_id' in request.session:
        user_id = request.session['user_id']
        try:
             user= UserProfile.objects.get(id=user_id)
             
             return render(request, 'home.html', { 'user': user})

        except UserProfile.DoesNotExist:
            return redirect('index')
     
    return redirect('index') 


def android_app_create_form(request):
    if request.method == 'POST':
        form = AndroidAppForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            messages.success(request, 'App added successfully')
            return redirect('dashboard')
    else:
        form = AndroidAppForm()

    return render(request, 'android_app_create_form.html', {'form': form})



def custom_logout(request):
    
    logout(request)
    return redirect('index')



def profile_view(request):
    if 'user_id' in request.session:
        user_id =request.session['user_id']

        user= UserProfile.objects.get(id=user_id)  
        return render(request, 'profile.html', {'user': user})


def app_detail(request, app_id):
    app = get_object_or_404(AndroidApp, id=app_id)

    if 'user_id' in request.session:
        user_id = request.session['user_id']
        user_profile = UserProfile.objects.get(id=user_id)

        if request.method == 'POST':
            form = ScreenshotUploadForm(request.POST, request.FILES)

            if form.is_valid():
                screenshot = form.cleaned_data['screenshot']
                completed = True if screenshot else False

                # Calculate the points earned for this task
                points_earned = app.points_earned if completed else 0

                # Create a UserTask instance
                task = UserTask.objects.create(
                    user=user_profile,
                    task_name=app.app_name,
                    task_category=app.category,
                    task_sub_category=app.sub_category,
                    completed=completed,
                    screenshot=screenshot
                )
                
                # Check if a UserPoints object already exists for this user and app
                user_points, created = UserPoints.objects.get_or_create(
                    user=user_profile,
                    android_app=app,
                    defaults={'points_earned': points_earned}  # Set points_earned when creating the object
                )

                # Handle the None case for points_earned
                if user_points.points_earned is None:
                    user_points.points_earned = points_earned
                else:
                    user_points.points_earned += points_earned

                user_points.save()

                # Calculate the total points earned by the user
                total_points = UserPoints.objects.filter(user=user_profile).aggregate(Sum('points_earned'))['points_earned__sum'] or 0

                
                user_points.total_points = total_points
                user_points.save()
             
                return redirect('user_points')  

        else:
            form = ScreenshotUploadForm()

    return render(request, 'app_detail.html', {'app': app, 'form': form})


def user_points(request):
    if 'user_id' in request.session:
        user_id = request.session['user_id']
        user_profile = UserProfile.objects.get(id=user_id)
        
        # Calculate the total points earned by the user
        total_points = UserPoints.objects.filter(user=user_profile).aggregate(Sum('points_earned'))['points_earned__sum'] or 0

        # Get the individual points earned by the user
        points = UserPoints.objects.filter(user=user_profile)
    else:
        total_points = 0
        points = []

    return render(request, 'points.html', {'points':points, 'total_points': total_points})





class AppListView(APIView):
    def get(self, request, format=None):
        queryset = AndroidApp.objects.all()
        serializer = AndroidAppSerializer(queryset, many=True)
       
        user_profile = None
        if 'user_id' in request.session:
            user_id = request.session['user_id']
            user_profile = UserProfile.objects.get(id=user_id)
        
        context = {
            'app_list': serializer.data,
            'user_profile': user_profile  
        }
        
        return render(request, 'dashboard.html', context, status=status.HTTP_200_OK)

