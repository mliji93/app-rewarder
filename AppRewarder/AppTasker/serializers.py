from rest_framework import serializers
from .models import UserProfile, AndroidApp, UserTask, UserPoints



class AndroidAppSerializer(serializers.ModelSerializer):
    class Meta:
        model = AndroidApp
        fields = '__all__'
