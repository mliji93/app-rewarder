from django import forms
from .models import UserProfile,AndroidApp

class RegistrationForm(forms.ModelForm):
    # Set the default value for the 'type' field
    

    class Meta:
        model = UserProfile
        fields = ['name', 'email', 'phone','username', 'password']



class AndroidAppForm(forms.ModelForm):
    class Meta:
        model = AndroidApp
        fields = '__all__'
        
class ScreenshotUploadForm(forms.Form):
    screenshot = forms.ImageField(
        label='Upload Screenshot',
        required=False,  # Allows the field to be empty
        widget=forms.FileInput(attrs={'accept': 'image/*'}),  # Restricts file types to images
    )